﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Player2048))]
public class PlayerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Player2048 player = (Player2048) target;

        if (EditorApplication.isPlaying && player.AllBricks != null && player.photonView.IsMine)
        {
            EditorGUILayout.LabelField("Current State");
            for (int i = 0;i < 7;i++)
            {
                GUILayout.BeginHorizontal();
                for (int j = 0;j < 5;j++)
                {
                    GUILayout.BeginVertical();

                    GUIStyle tableStyle = new GUIStyle("box");
                    tableStyle.fixedHeight = 25f;
                    tableStyle.fixedWidth = 50f;
                    GUILayout.Button(player.AllBricks [i,j].Number.ToString(),tableStyle);
                    GUILayout.EndVertical();
                }

                GUILayout.EndHorizontal();
            }
        }
    }
}