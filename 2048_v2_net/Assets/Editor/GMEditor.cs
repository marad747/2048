﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GM))]
public class GMEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GM gm = (GM) target;

        if (gm.PreviousState != null)
        {
            EditorGUILayout.LabelField("Previous State");
            for (int i = 0; i < 7; i++)
            {
                GUILayout.BeginHorizontal();
                for (int j = 0; j < 5; j++)
                {
                    GUILayout.BeginVertical();

                    GUIStyle tableStyle = new GUIStyle("box");
                    tableStyle.fixedHeight = 25f;
                    tableStyle.fixedWidth = 50f;
                    GUILayout.Button(gm.PreviousState[i, j].ToString(),tableStyle);
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();
            }
        }
        EditorGUILayout.Space();

        if (EditorApplication.isPlaying)
        {
            EditorGUILayout.LabelField("Current State");
            for (int i = 0; i < 7; i++)
            {
                GUILayout.BeginHorizontal();
                for (int j = 0; j < 5; j++)
                {
                    GUILayout.BeginVertical();

                    GUIStyle tableStyle = new GUIStyle("box");
                    tableStyle.fixedHeight = 25f;
                    tableStyle.fixedWidth = 50f;
                    //GUILayout.Button(gm.AllBricks[i, j].Number.ToString(), tableStyle);
                    GUILayout.EndVertical();
                }

                GUILayout.EndHorizontal();
            }
        }
    }
}
