﻿using UnityEngine;

public abstract class BrickBase : MonoBehaviour
{
    public int X, Y;
    [SerializeField]
    protected int _number;
    public GameObject _myGameObject;  

    public virtual void OnNumberChange()
    {
    }
}
