﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Player2048 : MonoBehaviourPunCallbacks, IPunObservable
{
    public PhotonView photonView;
    public Transform enemyBrickSpawnPoint;
    public GameObject CurrentenemyGameObject;
    public Vector3 EnemyGameFieldOffset;
    public bool IsRealPlayer = true;
    public Brick [,] AllBricks = new Brick [7,5];
    public List<Brick> EnptyBricks = new List<Brick>();

    public List<EnemyBrick> EnemyBricks = new List<EnemyBrick>();

    public List<Brick []> Columns = new List<Brick []>();
    public List<Brick []> Rows = new List<Brick []>();


    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();
        enemyBrickSpawnPoint = GameObject.FindGameObjectWithTag("enemyBrickSpawnPoint").transform;
        
        if (!IsRealPlayer)
        {
            var bricks = new List<Brick>();
            for (int i = 0; i < 7; i++)
            {
                int i1 = i;
                for (int j = 0; j < 5; j++)
                {
                    int j1 = j;
                    GameObject brick = Instantiate(Resources.Load<GameObject>("brick"),transform);
                    brick.GetComponent<Brick>().Init(i1,j1);
                    bricks.Add(brick.GetComponent<Brick>());
                }
            }
            
            foreach (var brick in bricks)
            {
                AllBricks [brick.X,brick.Y] = brick;
                EnptyBricks.Add(brick);
            }
            StartCoroutine(MakeBotDecision());
        }
        else
        {
            var bricks = FindObjectsOfType<Brick>();
            foreach (var brick in bricks)
            {
                AllBricks [brick.X,brick.Y] = brick;
                EnptyBricks.Add(brick);
            }
        }
        EnemyBricks = FindObjectsOfType<EnemyBrick>().ToList();
        CalculateRowsAndColumns();
    }

    private void CalculateRowsAndColumns()
    {
        Columns.Add(new Brick [] { AllBricks [0,0],AllBricks [1,0],AllBricks [2,0],AllBricks [3,0],AllBricks [4,0],AllBricks [5,0],AllBricks [6,0] });
        Columns.Add(new Brick [] { AllBricks [0,1],AllBricks [1,1],AllBricks [2,1],AllBricks [3,1],AllBricks [4,1],AllBricks [5,1],AllBricks [6,1] });
        Columns.Add(new Brick [] { AllBricks [0,2],AllBricks [1,2],AllBricks [2,2],AllBricks [3,2],AllBricks [4,2],AllBricks [5,2],AllBricks [6,2] });
        Columns.Add(new Brick [] { AllBricks [0,3],AllBricks [1,3],AllBricks [2,3],AllBricks [3,3],AllBricks [4,3],AllBricks [5,3],AllBricks [6,3] });
        Columns.Add(new Brick [] { AllBricks [0,4],AllBricks [1,4],AllBricks [2,4],AllBricks [3,4],AllBricks [4,4],AllBricks [5,4],AllBricks [6,4] });


        Rows.Add(new Brick [] { AllBricks [0,0],AllBricks [0,1],AllBricks [0,2],AllBricks [0,3],AllBricks [0,4] });
        Rows.Add(new Brick [] { AllBricks [1,0],AllBricks [1,1],AllBricks [1,2],AllBricks [1,3],AllBricks [1,4] });
        Rows.Add(new Brick [] { AllBricks [2,0],AllBricks [2,1],AllBricks [2,2],AllBricks [2,3],AllBricks [2,4] });
        Rows.Add(new Brick [] { AllBricks [3,0],AllBricks [3,1],AllBricks [3,2],AllBricks [3,3],AllBricks [3,4] });
        Rows.Add(new Brick [] { AllBricks [4,0],AllBricks [4,1],AllBricks [4,2],AllBricks [4,3],AllBricks [4,4] });
        Rows.Add(new Brick [] { AllBricks [5,0],AllBricks [5,1],AllBricks [5,2],AllBricks [5,3],AllBricks [5,4] });
        Rows.Add(new Brick [] { AllBricks [6,0],AllBricks [6,1],AllBricks [6,2],AllBricks [6,3],AllBricks [6,4] });
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        
    }
        
    [PunRPC]
    private void Win(int id)
    {
        if (photonView.IsMine)
        {
            if (id != photonView.ViewID)
            {
                GM.Instance.ShowLosePanel();
            }
            else
            {
                GM.Instance.ShowGameWin();
                DataManager.Instance.AddCoins();
            }
        }
    }

    [PunRPC]
    private void SendData(string userData)
    {
        List<Data> data = JsonUtility.FromJson<DataWraper>(userData).DataWrap;
        UpdateEnemyBricks(data);
    }

    private void UpdateEnemyBricks(List<Data> data)
    {
        foreach (var d in data)
        {
            Data dat = d;
            foreach (var brick in EnemyBricks)
            {
                if (brick.X == d.X && brick.Y == d.Y)
                {
                    brick.Number = d.Number;
                    break;
                }
            }
        }
    }

    private IEnumerator MakeBotDecision()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            int value = GM.Instance.GetAvaibleStartValue();
            int columnIndex = Random.Range(0, Columns.Count);
            GM.Instance.InstertBrickOnField_Bot(columnIndex,Columns,Rows,value);

            yield return new WaitForSeconds(1f);

            List<Data> userData = new List<Data>();

            foreach (var brick in EnptyBricks)
            {
                userData.Add(new Data(brick.X,brick.Y,brick.Number));
            }
            DataWraper dataWraper = new DataWraper(userData);

            string dataWrapper = JsonUtility.ToJson(dataWraper);

            List<Data> data = JsonUtility.FromJson<DataWraper>(dataWrapper).DataWrap;
            UpdateEnemyBricks(data);
        }
    }

    void Update()
    {
        if (IsRealPlayer && photonView.IsMine)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(touchPoint, Vector2.zero);

                if (hit.collider != null)
                {
                    var brick = hit.transform.GetComponent<Brick>();
                    if (brick != null)
                    {
                        GM.Instance.InstertBrickOnField_Player(brick.Y, brick.X, Columns, Rows);
                    }
                }
            }
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        StartCoroutine(Leave());
    }

    IEnumerator Leave()
    {
        yield return new WaitForSeconds(1f);
        DataManager.Instance.AddCoins();
        GM.Instance.ShowGameWin();
    }
}
