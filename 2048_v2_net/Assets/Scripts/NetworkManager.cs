﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager Instance;
    public bool LoadGame;
    public float DefaultSearchTime = 5f;
    public bool IsOnlineGame = false;
    public float CurrentLeftTime = 0;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            Connect();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("connect to master, try connect to lobby");
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnJoinedLobby()
    {
        //JoinRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom(Guid.NewGuid().ToString(), null, TypedLobby.Default);
    }

    // Update is called once per frame
    private void Update()
    {
        if (LoadGame)
            return;

        if (PhotonNetwork.InRoom)
        {
            if (PhotonNetwork.CurrentRoom != null)
            {
                CurrentLeftTime += Time.deltaTime;

                if (PhotonNetwork.CurrentRoom.Players.Count == 2)
                {
                    IsOnlineGame = true;
                    LoadGame = true;
                    PhotonNetwork.LoadLevel("SampleScene");
                }

                if (CurrentLeftTime >= DefaultSearchTime)
                {
                    IsOnlineGame = false;
                    LoadGame = true;
                    PhotonNetwork.LoadLevel("SampleScene");
                }
            }
        }
    }

    public void JoinRoom()
    {             //Debug.Log("Connect to lobby, seting name");
#if UNITY_EDITOR
        PhotonNetwork.NickName = "Unity_player";
#else
        PhotonNetwork.NickName = "Android_player";;
#endif

        PhotonNetwork.JoinRandomRoom();
    }

    private void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.NetworkClientState.ToString());
        GUILayout.Label(PhotonNetwork.NetworkingClient.CloudRegion);
    }

    public void Connect()
    {
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }
}