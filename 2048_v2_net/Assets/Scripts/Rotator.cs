﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float RotationSpeed;
    public RectTransform TargetTransform;
    void Start()
    {
        TargetTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        TargetTransform.Rotate(0f,0f,RotationSpeed * Time.deltaTime);
    }
}
