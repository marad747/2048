﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public List<Booster> BoostersLockImage = new List<Booster>();
    public GameObject BoosterAcceptor;
    public Button BoosterYes, BoosterNo;

    public GameObject BoosterBuyPanel;
    public Button BoosterYesBuy, BoosterNoBuy;
    public TextMeshProUGUI BuyText;
    public TextMeshProUGUI CoinsText;

    private void Start()
    {
        GM.Instance.OnBoosterChange += OnBoosterChange;
        DataManager.Instance.OnCoinChange += UpdateCoinsText;
        CoinsText.text = DataManager.Instance.PlayerData.Coins.ToString();
    }

    private void OnBoosterChange(BoosterType type)
    {
        if (type != BoosterType.None)
        {
            foreach (var item in BoostersLockImage)
            {
                item.LockBooster();
            }
        }
        else
        {
            foreach (var item in BoostersLockImage)
            {
                item.ResetBooster();
            }
        }
    }

    public void InitBoosterPanel(Action boosterYes,Action boosterNo)
    {
        BoosterYes.onClick.RemoveAllListeners();
        BoosterNo.onClick.RemoveAllListeners();

        BoosterYes.onClick.AddListener(() =>
        {
            boosterYes.Invoke();
            BoosterAcceptor.SetActive(false);
        });

        BoosterNo.onClick.AddListener(() =>
        {
            boosterNo.Invoke();
            BoosterAcceptor.SetActive(false);
        });

        BoosterAcceptor.SetActive(true);
    }

    public void UpdateCoinsText(int coins)
    {
        CoinsText.text = coins.ToString();
    }

    public void InitBoosterBuyPanel(BoosterType type, int count, int price, Action<int> OnBuy)
    {
        BoosterYesBuy.onClick.RemoveAllListeners();
        BoosterNoBuy.onClick.RemoveAllListeners();
        GM.Instance.CanClick = false;
        string text = string.Format("Do you really want buy {0} booster(x3) for {1} coins?", type.ToString(),
            price.ToString());

        BuyText.text = text;

        BoosterYesBuy.onClick.AddListener(() =>
        {
            DataManager.Instance.BuyBooster(type, count,price,OnBuy);
            GM.Instance.CanClick = true;
            BoosterBuyPanel.SetActive(false);
        });

        BoosterNoBuy.onClick.AddListener(()=>
        {
            GM.Instance.CanClick = true;
            BoosterBuyPanel.SetActive(false);
        });

        BoosterBuyPanel.SetActive(true);
    }
}
