﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickRemaner : MonoBehaviour
{  
    [ContextMenu("RemaneBrick")]
    private void RemaneBrick()
    {
        var bricks = FindObjectsOfType<Brick>();
        for (int i = 0; i < bricks.Length; i++)
        {
            var brick = bricks[i];
            brick.gameObject.name = "brick_" + brick.X + "_" + brick.Y;
            brick.Number = 0;
        }
    }

    public GameObject brickPrefab;

    [ContextMenu("EnemyBlock")]
    private void CreateEnemyBlocks()
    {
        var enemyBricks = FindObjectsOfType<EnemyBrick>();

        foreach (var brick in enemyBricks)
        {
            var go = Instantiate(brickPrefab,brick.gameObject.transform);
            go.transform.localPosition = Vector3.zero;
        }
    }

}
