﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkController : MonoBehaviour
{
    public Button StartGame,Quit;
    public GameObject SearchOppPanel;
    void Start()
    {
        StartGame.onClick.AddListener(() => SearchOppPanel.SetActive(true));
        StartGame.onClick.AddListener(()=>NetworkManager.Instance.JoinRoom());
        Quit.onClick.AddListener(Application.Quit);
        NetworkManager.Instance.LoadGame = false;
        //NetworkManager.Instance.Connect();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
