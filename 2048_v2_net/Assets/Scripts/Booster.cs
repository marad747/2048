﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Booster : MonoBehaviour
{
    public BoosterType Type;
    public bool Active = false;
    public GameObject LockedSpriteObj;
    public TextMeshProUGUI Counter;
    public int Price;
    public int DefaultBuyCount = 3;

    void Awake()
    {
        DataManager.Instance.OnChangeBooster += OnChangeBooster;
    }

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if (DataManager.Instance.CheckBoosterAvaible(Type))
            {
                GM.Instance.SetBoostType(!Active ? Type : BoosterType.None);
            }
            else if (DataManager.Instance.CheckCoins(Price))
            {
                FindObjectOfType<UIManager>().InitBoosterBuyPanel(Type,DefaultBuyCount,Price,UpdateBoosterCount);
            }

        });
    }

    private void UpdateBoosterCount(int count)
    {
        Counter.text = count.ToString();
    }

    public void OnChangeBooster(BoosterType booster, int count)
    {
        if (Type == booster)
        {
            Counter.text = count.ToString();
        }
    }

    public void ResetBooster()
    {
        Active = false;
        LockedSpriteObj.SetActive(false);
    }

    public void LockBooster()
    {
        Active = true;
        LockedSpriteObj.SetActive(true);
    }
}

public enum BoosterType
{
    None,
    DeleteSelected,
    StepBack
}
