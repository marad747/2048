﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;
    public PlayerData PlayerData;
    public const string PLAYER_DATA = "PlayerData";
    public event Action<BoosterType, int> OnChangeBooster;
    public event Action<int> OnCoinChange;
    public int DefaultWinCoins;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        if (PlayerPrefs.HasKey(PLAYER_DATA))
        {
            PlayerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString(PLAYER_DATA));
        }
        else
        {
            PlayerData = new PlayerData
            {
                PlayerBoosterData = new List<BoosterData>
                {
                    new BoosterData(BoosterType.DeleteSelected, 5), new BoosterData(BoosterType.StepBack, 5)
                }
            };
            PlayerData.Coins = 100;
            SavePlayerData();
        }
        OnCoinChange?.Invoke(PlayerData.Coins);

    }

    public bool CheckCoins(int coins)
    {
        return PlayerData.Coins >= coins;
    }

    public bool CheckBoosterAvaible(BoosterType booster)
    {
        foreach (var b in PlayerData.PlayerBoosterData)
        {
            if (b.Booster == booster && b.Count >= 1)
            {
                return true;
            }
        }

        return false;
    }

    public void SavePlayerData()
    {
        PlayerPrefs.SetString(PLAYER_DATA,JsonUtility.ToJson(PlayerData));
    }

    public void UseBooster(BoosterType booster)
    {
        foreach (var b in PlayerData.PlayerBoosterData)
        {
            if (b.Booster == booster)
            {
                b.Count--;
                OnChangeBooster?.Invoke(b.Booster,b.Count);
                SavePlayerData();
            }
        }
    }

    public void UpdateBoosterData()
    {
        foreach (var b in PlayerData.PlayerBoosterData)
        {
            OnChangeBooster?.Invoke(b.Booster,b.Count);
        }
    }

    [ContextMenu("give boosters")]
    private void GiveBoosters()
    {
        foreach (var b in PlayerData.PlayerBoosterData)
        {
            b.Count = 10;
        }

        SavePlayerData();
        UpdateBoosterData();
    }

    [ContextMenu("Deleta data")]
    private void Delete()
    {
        PlayerPrefs.DeleteAll();
    }

    public void BuyBooster(BoosterType type, int count, int price, Action<int> OnBuy)
    {
        foreach (var booster in PlayerData.PlayerBoosterData)
        {
            if (booster.Booster == type)
            {
                booster.Count += count;
                PlayerData.Coins -= price;
                OnBuy?.Invoke(booster.Count);
            }
        }
        SavePlayerData();
        OnCoinChange?.Invoke(PlayerData.Coins);
    }

    public void AddCoins()
    {
        PlayerData.Coins += DefaultWinCoins;
        SavePlayerData();
        OnCoinChange?.Invoke(PlayerData.Coins);
    }
}

[System.Serializable]
public class PlayerData
{
    public List<BoosterData> PlayerBoosterData = new List<BoosterData>();
    public int Coins;
}

[System.Serializable]
public class BoosterData
{
    public BoosterType Booster;
    public int Count;

    public BoosterData(BoosterType booster,int count)
    {
        Booster = booster;
        Count = count;
    }
}
