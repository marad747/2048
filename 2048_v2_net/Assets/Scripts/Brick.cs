﻿using TMPro;
using UnityEngine;

public class Brick : BrickBase
{

    public GameObject MyGameObject
    {
        get
        {
            return _myGameObject;
        }
        set
        {
            _myGameObject = value;
            OnGameObjectChange();
        }
    }   

    public int Number
    {
        get
        {
            return _number;
        }
        set
        {
            _number = value;
            OnNumberChange();   
        }
    }

    //private void OnMouseDown()
    //{
    //    GM.Instance.InstertBrickOnField(Y,X);
    //}
    public void Init(int x, int y)
    {
        X = x;
        Y = y;
        Number = 0;
    }

    public override void OnNumberChange()
    {
        if (_number != 0)
        {
            
        }
    }

    private void OnGameObjectChange()
    {
        if (_number != 0)
        {
            MyGameObject.GetComponentInChildren<TextMeshPro>().text = Number.ToString();
            MyGameObject.transform.SetParent(transform);
            MyGameObject.transform.localPosition = Vector2.zero;
        }
    }

    public void Destroy()
    {
        if(_myGameObject != null)
        {   
            GM.Instance.CreateEffect(transform.position);
            Destroy(_myGameObject);
        }
    }

    public void RestoreAfter1StepBack(int N)
    {
        Number = N;
        if (MyGameObject != null)
        {
            MyGameObject.GetComponentInChildren<TextMeshPro>().text = N.ToString();
        }
    }
}
