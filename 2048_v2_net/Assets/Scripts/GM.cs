﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GM : MonoBehaviour
{
    public static GM Instance;

    public GameObject CurrentBrick;
    public int CurrentValue;
    public GameObject BrickPrefab;
    public float delay;
    public int[,] PreviousState = new int[7,5];

    public List<bool> ColumnMove = new List<bool>()
    {
        true,
        true,
        true,
        true,
        true
    };

    public List<bool> RowsMove = new List<bool>()
    {
        true,
        true,
        true,
        true,
        true,
        true,
        true
    };

    public bool CanClick = true;

    public List<int> AvaibleStartValues = new List<int>();

    public GameObject EffectPrefab;

    public Player2048 MyPlayer;
    public Player2048 EnemyPlayer;

    public GameObject WinPanel;
    public GameObject LoosePanel;

    public List<Button> RestartButtons = new List<Button>();

    public BoosterType CurrentBoosType = BoosterType.None;
    public event Action<BoosterType> OnBoosterChange;
    public UIManager UIManager;

    private void Awake()
    {
        Instance = this;
    }

    IEnumerator Start()
    {
        MyPlayer = PhotonNetwork.Instantiate("Player2048",Vector3.zero,Quaternion.identity).GetComponent<Player2048>();

        if (NetworkManager.Instance.IsOnlineGame)
        {
            yield return StartCoroutine(WaitPlayers());
        }
        else
        {
            EnemyPlayer = PhotonNetwork.Instantiate("Player2048",Vector3.zero,Quaternion.identity).GetComponent<Player2048>();
            EnemyPlayer.IsRealPlayer = false;
        }

        foreach (var button in RestartButtons)
        {   
            button.onClick.AddListener(()=> SceneManager.LoadScene("StartScene"));
        }

        var bricks = FindObjectsOfType<Brick>();

        CreateRandomValue();
        DataManager.Instance.UpdateBoosterData();
    }

    IEnumerator WaitPlayers()
    {
        while (MyPlayer == null || EnemyPlayer == null)
        {
            yield return new WaitForSeconds(1f);
            if (FindObjectsOfType<Player2048>().Length != 0)
            {
                EnemyPlayer = FindObjectsOfType<Player2048>().First((x) => !x.GetComponent<PhotonView>().IsMine);
            }
        }                         
    }

    private void CreateRandomValue()
    {
        CurrentBrick = Instantiate(BrickPrefab);
        CurrentValue = GetAvaibleStartValue();
        CurrentBrick.GetComponentInChildren<TextMeshPro>().text = CurrentValue.ToString();
    }

    public int GetAvaibleStartValue()
    {
        return AvaibleStartValues [Random.Range(0,AvaibleStartValues.Count - 1)];
    }

    public void InstertBrickOnField_Bot(int columnIndex, List<Brick[]> Columns, List<Brick[]> Rows, int value)
    {
        if (Columns [columnIndex] [Columns [columnIndex].Length - 1].Number == 0)
        {
            Columns [columnIndex] [Columns [columnIndex].Length - 1].Number = value;
            Move(Columns,Rows, true);
        }
        else
        {
            Debug.Log("Paste error into " + columnIndex);
        }
    }

    public void InstertBrickOnField_Player(int columnIndex, int rowIndex,List<Brick []> Columns,List<Brick []> Rows)
    {       
        if (!CanClick)
        {
            return;
        }

        RememberPreviousState();
        if (CurrentBoosType == BoosterType.None)
        {
            if (Columns[columnIndex][Columns[columnIndex].Length - 1].Number == 0)
            {
                Columns[columnIndex][Columns[columnIndex].Length - 1].Number = CurrentValue;
                Columns[columnIndex][Columns[columnIndex].Length - 1].MyGameObject = CurrentBrick;
                Move(Columns,Rows);
            }
        }
        else if (CurrentBoosType == BoosterType.DeleteSelected)
        {
            MyPlayer.AllBricks [rowIndex,columnIndex].Number = 0;
            if (MyPlayer.AllBricks [rowIndex, columnIndex].MyGameObject != null)
            {
                MyPlayer.AllBricks [rowIndex, columnIndex].Destroy();
            }

            Move(Columns,Rows);
            DataManager.Instance.UseBooster(BoosterType.DeleteSelected);
        }
    }

    private void RememberPreviousState()
    {
        PreviousState = new int[7,5];
        var AllBricks = MyPlayer.AllBricks;
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                PreviousState[i, j] = AllBricks[i, j].Number;
            }
        }
    }

    public void Move(List<Brick []> Columns,List<Brick []> Rows, bool isBotAction = false)
    {
        StartCoroutine(MoveItems(Columns,Rows,isBotAction));
    }

    IEnumerator MoveItems(List<Brick []> Columns,List<Brick []> Rows, bool isBotAction = false)
    {
        if (!isBotAction)
        {
            CanClick = false;
        }

        while (CheckEmptyItems(Columns))
        {
            for (int i = 0; i < Columns.Count; i++)
            {
                StartCoroutine(MoveColumn(Columns[i], i,isBotAction));
            }

            yield return new WaitWhile(() => ColumnMove.Contains(false));

            for (int i = 0; i < Rows.Count; i++)
            {
                StartCoroutine(MoveRow(Rows[i], i,isBotAction));
            }

            yield return new WaitWhile(() => RowsMove.Contains(false));
        }

        if (!isBotAction)
        {
            SendData();
            CheckWin();
            
        }

        if (CurrentBoosType == BoosterType.None)
        {
            if (!isBotAction)
            {
                CreateRandomValue();
            }
        }

        if (!isBotAction)
        {
            CanClick = true;
            CurrentBoosType = BoosterType.None;
            OnBoosterChange?.Invoke(CurrentBoosType);
        }
    }

    private void SendData()
    {
        List<Data> userData = new List<Data>();
        foreach (var brick in MyPlayer.EnptyBricks)
        {
           userData.Add(new Data(brick.X,brick.Y,brick.Number));
        }
        DataWraper dataWraper = new DataWraper(userData);

        string dataWrapper  = JsonUtility.ToJson(dataWraper);
        if (NetworkManager.Instance.IsOnlineGame)
        {
            EnemyPlayer.photonView.RPC("SendData", RpcTarget.Others, dataWrapper);
        }
    }

    private void CheckWin()
    {
        foreach (var brick in MyPlayer.EnptyBricks)
        {
            if (brick.Number == 2048)
            {
                if (NetworkManager.Instance.IsOnlineGame)
                {
                    EnemyPlayer.photonView.RPC("Win", RpcTarget.All, MyPlayer.photonView.ViewID);
                }
                else
                {
                    ShowGameWin();
                    DataManager.Instance.AddCoins();
                }
                //MyPlayer.photonView.RPC("Win",RpcTarget.All,MyPlayer.photonView.ViewID);
            }
        }

        bool endGame = true;
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                if (MyPlayer.AllBricks [i, j].Number == 0)
                {
                    endGame = false;
                    break;
                }
            }
        }

        if (endGame)
        {
            if (NetworkManager.Instance.IsOnlineGame)
            {
                EnemyPlayer.photonView.RPC("Win", RpcTarget.Others, EnemyPlayer.photonView.ViewID);
            }

            ShowLosePanel();
        }
    }

    private IEnumerator MoveColumn(Brick[] bricks,int columnIndex, bool isBotAction = false)
    {
        ColumnMove[columnIndex] = false;
        yield return new WaitForSeconds(delay);
        while (CanMoveUp(bricks,isBotAction))
        {
            yield return new WaitForSeconds(delay);
        }
        ColumnMove [columnIndex] = true;
    }  

    private IEnumerator MoveRow(Brick [] bricks,int rowIndex, bool isBotAction = false)
    {
        RowsMove [rowIndex] = false;
        yield return new WaitForSeconds(delay);
        while (CanMoveRight(bricks,isBotAction))
        {
            yield return new WaitForSeconds(delay);
        }
        RowsMove [rowIndex] = true;
    }

    private bool CanMoveUp(Brick[] bricks, bool isBotAction = false)
    {
        for (int i = 0; i < bricks.Length-1; i++)
        {
            if (bricks[i].Number == 0 && bricks[i + 1].Number != 0)
            {
                bricks [i].Number = bricks [i + 1].Number;
                bricks [i + 1].Number = 0;

                if (!isBotAction)
                {
                    bricks[i].MyGameObject = bricks[i + 1].MyGameObject;
                    bricks[i + 1].MyGameObject = null;
                }

                return true;
            }

            if (bricks [i].Number != 0 && bricks [i].Number == bricks [i + 1].Number)
            {
                bricks [i].Number *= 2;
                bricks [i + 1].Number = 0;

                if (!isBotAction)
                {
                    bricks[i].Destroy();
                    bricks[i].MyGameObject = bricks[i + 1].MyGameObject;
                    bricks[i + 1].MyGameObject = null;
                }

                return true;
            }
        }
        return false;
    }

    private bool CanMoveRight(Brick [] bricks, bool isBotAction = false)
    {
        for (int i = bricks.Length - 1;i > 0;i--)
        {
            if (bricks [i].Number != 0 && bricks [i].Number == bricks [i - 1].Number)
            {
                bricks [i].Number *= 2;                                                             
                bricks [i - 1].Number = 0;

                if (!isBotAction)
                {
                    bricks[i].Destroy();
                    bricks[i].MyGameObject = bricks[i - 1].MyGameObject;
                    bricks[i - 1].MyGameObject = null;
                }

                return true;
            }
        }
        return false;
    }

    private bool CheckEmptyItems(List<Brick []> Columns)
    {
        for (int i = 0;i < Columns.Count;i++)
        {
            for (int j = Columns [i].Length - 1;j > 0;j--)
            {
                if (( Columns [i] [j].Number != 0 && Columns [i] [j - 1].Number == 0 ) || ( Columns [i] [j].Number != 0 && Columns [i] [j].Number == Columns [i] [j - 1].Number ))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void CreateEffect(Vector3 position)
    {   
        var effect = Instantiate(EffectPrefab, position-Vector3.forward,Quaternion.identity);
        Destroy(effect,1.3f);
    }
     
    public void ShowGameWin()
    {
        PhotonNetwork.LeaveRoom();
        WinPanel.SetActive(true);
    }

    public void ShowLosePanel()
    {
        PhotonNetwork.LeaveRoom();
        LoosePanel.SetActive(true);
    }

    public void SetBoostType(BoosterType type)
    {
        if (type == BoosterType.DeleteSelected)
        {
            CurrentBoosType = type;
            OnBoosterChange?.Invoke(CurrentBoosType);
        }
        else if (type == BoosterType.StepBack)
        {
            if (PreviousState != null)
            {
                CanClick = false;
                UIManager.InitBoosterPanel(Make1StepBack, () =>
                {
                    CanClick = true;
                    CurrentBoosType = BoosterType.None;
                });
                
            }
        }
    }

    private void Make1StepBack()
    {
        for (int i = 0;i < 7;i++)
        {
            for (int j = 0;j < 5;j++)
            {
                if (PreviousState [i,j] == 0)
                {
                    Destroy(MyPlayer.AllBricks [i,j].MyGameObject);
                }
                else
                {
                    if (MyPlayer.AllBricks [i,j].MyGameObject == null)
                    {
                        MyPlayer.AllBricks [i,j].MyGameObject = Instantiate(BrickPrefab,Vector3.zero,Quaternion.identity,MyPlayer.AllBricks [i,j].gameObject.transform);
                        MyPlayer.AllBricks [i,j].MyGameObject.transform.localPosition = Vector3.zero;
                    }
                }
                MyPlayer.AllBricks [i,j].RestoreAfter1StepBack(PreviousState [i,j]);
            }
        }
        PreviousState = null;
        SendData();
        CanClick = true;
        DataManager.Instance.UseBooster(BoosterType.StepBack);
    }
}


[System.Serializable]
public class Data
{
    public int X;
    public int Y;
    public int Number;

    public Data(int x,int y, int number)
    {
        X = x;
        Y = y;
        Number = number;
    }
}

[System.Serializable]
public class DataWraper
{
    public List<Data> DataWrap = new List<Data>();

    public DataWraper(List<Data> data)
    {
        DataWrap = data;
    }
}
