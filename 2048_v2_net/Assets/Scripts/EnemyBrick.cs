﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyBrick : BrickBase
{
    public TextMeshPro TmText;

    public int Number
    {
        get
        {
            return _number;
        }
        set
        {
            _number = value;
            OnNumberChange();
        }
    }

    public override void OnNumberChange()
    {
        if (_number == 0)
        {
            _myGameObject.SetActive(false);
        }

        else
        {
            _myGameObject.SetActive(true);
            TmText.text = _number.ToString();
        }                                    
    }

    private void Start()
    {
        _myGameObject = transform.GetChild(0).gameObject;
        TmText = GetComponentInChildren<TextMeshPro>();
        _myGameObject.SetActive(false);
    }

    public void SetXY(int x, int y)
    {
        X = x;
        Y = y;
    }



}
